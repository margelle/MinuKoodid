package ee.bcs.koolitu.threads;

public class ExampleRunnableSync implements Runnable
{
	static int i = 0;
	@Override
	public void run(){
		synchronized (this){		//ennem andis segases jrk-s, sünkroniseerimine kindlustab, et kogu aeg ilmub samas jrk-s
			

		i++;
		System.out.println(Thread.currentThread().getName() + " i = " + i);

		}
}
}
