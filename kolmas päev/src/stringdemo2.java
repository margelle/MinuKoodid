
public class stringdemo2 {

	 public static void main(String[] args) {
		 
		 String neli = "44"; // see on tekst​
		 int iS = Integer.parseInt(neli); // OK
		 System.out.println(iS); 
		 //iS = Integer.parseInt("tere"); // viga täitmisel​
		 //String vastus = iS.toString(); // viga -  pole klass​
		 String vastus1 = ((Integer)iS).toString(); // OK
		 System.out.println(vastus1);
		 // klassidel on enamasti meetod, kuidas teksti sellesse​
		 // klassi teisendada (parseX) ja kuidas klassi​
		 // stringiks teisendada (toString)​		 ​    	  
	 }
}
