
public class StringFormat {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
String nimedeLoeteluTekst = "See on loetelu nimedest: Tallinn, Tartu, Pärnu, Kuressaare";
String[] splitKooloniga = nimedeLoeteluTekst.split(":");
System.out.println("SplitKooloniga massiivis on " 
		+ splitKooloniga.length + " elementi");
System.out.println("Esimene element on :\"" 
		+ splitKooloniga[0] +"\"");
System.out.println("Teine element on :\"" 
		+ splitKooloniga[1].trim() +"\"");
// see oli esimene split, nüüd tuleb teine split. Eesmärgiks saada linnad ükshaaval kätte.

String[] loeteluNimedest = splitKooloniga[1]
		.split(",");
// tekkis uus massiiv, mis koosneb 4 elemendist

System.out.println("loetelu Nimedest massiivis on " 
		+ loeteluNimedest.length + " elementi");
System.out.println("Esimene element on :\"" 
		+ loeteluNimedest[0].trim() + "\"");
System.out.println("Teine element on :\"" 
		+ loeteluNimedest[1] + "\"");
System.out.println("Kolmas element on : \""
		+ loeteluNimedest[2] + "\"");
System.out.println("Neljas element on :\""
		+ loeteluNimedest[3] + "\"");	
	}

}
