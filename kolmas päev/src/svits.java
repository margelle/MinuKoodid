
public class svits {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String color = "rose";
		
		if (color == "green") {
			System.out.println("Driver can drive a car.");
		} else if (color == "yellow") {
			System.out.println("Driver has to be ready to stop the car to start driving.");

		} else if (color == "red") {
			System.out.println("Driver has to stop car and wait for green light.");
		}
	
		System.out.println("-----Switch----");
		switch (color) {
		case "green":
			System.out.println("Driver can drive a car.");
			break;
		case "yellow":
			System.out.println("Driver has to be ready to stop the car to start driving.");
			break;
		case "red":
			System.out.println("Driver has to stop car and wait for green light.");
			break;
		default:
			System.out.println("You can fly!");
		}
	}
}
