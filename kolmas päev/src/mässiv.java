
public class mässiv {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	String[] nimed = { "Mati", "Kati", "Mari"};
	System.out.println(nimed[0] +" " + nimed[1] + " " + nimed[2]);
	
	
	// 3 realine ja 2 veeruline tabel; kohe loomisel paned andmed sisse; 
	// kohe peab panema kandilised sulud ja nimi ja loogsulgudega määrad ära infoväljad.
	
	String[][] tabel = {{"Mati", "Tallinn"}, {"Kati","Pärnu"}, {"Mari", "Narva"}};	
	System.out.println(tabel[0][0] + " " + tabel[0][1]);
	System.out.println(tabel[1][0] + " " + tabel[1][1]);
	System.out.println(tabel[2][0] + " " + tabel[2][1]);
	
	System.out.println(tabel[0][0] + " " + "linnast" + " " + tabel[0][1] + ".");
		
	// ükshaaval kirjeldatud tabel
	
	String[][] tabel2 = new String[3][2]; 
	tabel2[0][0] = "Mati";
	tabel2[0][1] = "Tallinn";
	tabel2[1][0] = "Kati";
	tabel2[1][1] = "Pärnu";
	tabel2[2][0] = "Mari";
	tabel2[2][1] = "Narva";
	
	System.out.println(tabel2[0][0] + " " + tabel2[0][1]);
	System.out.println(tabel2[1][0] + " " + tabel2[1][1]);
	System.out.println(tabel2[2][0] + " " + tabel2[2][1]);

	System.out.println(tabel2[0][0] + " " + "linnast" + " " + tabel2[0][1] + ".");
	}
}
