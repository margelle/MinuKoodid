package ee.bsc.koolitus.interfaceMain;

import ee.bsc.koolitus.interfacedemo.IShape;
import ee.bsc.koolitus.interfacedemo.Triangle;

public class MainInterface
{

	public static void main(String[] args)
	{
		Triangle triangle = new Triangle();
		triangle.setCentreCoordinates(2, 3);
		System.out.println(triangle.getCentrePoint());
		triangle.setSideALength(3.0); 
		triangle.setSideBLength(4.0);
		triangle.setSideCLength(5.5);
		System.out.println(triangle.getPerimeter());
	
		
		
	}

}
