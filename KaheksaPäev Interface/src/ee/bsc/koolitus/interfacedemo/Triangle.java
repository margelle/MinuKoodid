package ee.bsc.koolitus.interfacedemo;

import java.awt.Point;

public class Triangle implements IShape
{
	public Point centrePoint = new Point();
	
	@Override
	public void setCentreCoordinates(int x, int y)
	{
		centrePoint.setLocation(x, y);
		}

	public Point getCentrePoint()
	{
		return centrePoint;
	}

	public void setCentrePoint(Point centrePoint)
	{
		this.centrePoint = centrePoint;
	}
	private double sideALength = 0; //annan kõikidele külgedele väärtused 0 ja get+set
	public double getSideALength()
	{
		return sideALength;
	}

	public void setSideALength(double sideALength)
	{
		this.sideALength = sideALength;
	}

	public double getSideBLength()
	{
		return sideBLength;
	}

	public void setSideBLength(double sideBLength)
	{
		this.sideBLength = sideBLength;
	}

	public double getSideCLength()
	{
		return sideCLength;
	}

	public void setSideCLength(double sideCLength)
	{
		this.sideCLength = sideCLength;
	}
	private double sideBLength = 0;
	private double sideCLength = 0;

	public double getPerimeter() //so automaatselt tekitatud abst meetod, sest Shapi classis on loodud uus abtr meetod
	{
		return(sideALength +sideBLength +sideCLength); //liidab küljed kokku ja annab väljundi
		}
}
