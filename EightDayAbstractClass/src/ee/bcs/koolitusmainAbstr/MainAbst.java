package ee.bcs.koolitusmainAbstr;

import ee.bsc.koolitus.abstarctclass.Triangle;

public class MainAbst
{

	public static void main(String[] args)
	{
		Triangle triangle = new Triangle();
		System.out.println(triangle.DEFAULT_COLOR);
		triangle.setCentreCoordinates(1, 5);		//
		System.out.println(triangle.getCentrePoint().toString());
		triangle.setSideALength(2.0); //siin on antud külgedele pikkused
		triangle.setSideBLength(3.0);
		triangle.setSideCLength(2.5);
		System.out.println(triangle.getPerimeter());
	}
	
}
