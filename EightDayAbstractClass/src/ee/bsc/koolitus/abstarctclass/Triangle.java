package ee.bsc.koolitus.abstarctclass;

public class Triangle extends Shape{
	private double sideALength = 0; //annan kõikidele külgedele väärtused 0 ja get+set
	private double sideBLength = 0;
	private double sideCLength = 0;
	
	//getterid ja setterid külgede pikkuse määramiseks
	public double getSideALength()
	{
		return sideALength;
	}

	public void setSideALength(double sideALength)
	{
		this.sideALength = sideALength;
	}

	public double getSideBLength()
	{
		return sideBLength;
	}

	public void setSideBLength(double sideBLength)
	{
		this.sideBLength = sideBLength;
	}

	public double getSideCLength()
	{
		return sideCLength;
	}

	public void setSideCLength(double sideCLength)
	{
		this.sideCLength = sideCLength;
	}
	
@Override
public double getPerimeter() //so automaatselt tekitatud abst meetod, sest Shapi classis on loodud uus abtr meetod
{
	return(sideALength +sideBLength +sideCLength); //liidab küljed kokku ja annab väljundi
	}


}