package ee.bsc.koolitus.abstarctclass;

import java.awt.Point;

public abstract class Shape
{
	public final String DEFAULT_COLOR = "green";
	private Point centrePoint = new Point(); // getter ja setter, sest on vaja
												// objekt defineerida
//tavaline meetod
	public void setCentreCoordinates(int x, int y) // uus meetod tegime
	{
		centrePoint.setLocation(x, y);
	}

	//abst meetod
	public abstract double getPerimeter();
	
	public Point getCentrePoint()
	{
		return centrePoint;
	}

	public void setCentrePoint(Point centrePoint)
	{
		this.centrePoint = centrePoint;
	}

}

