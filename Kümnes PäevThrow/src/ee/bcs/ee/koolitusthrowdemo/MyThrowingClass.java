package ee.bcs.ee.koolitusthrowdemo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MyThrowingClass
{
	public void readMyLittleFile() throws FileNotFoundException, IOException, IncorrectStringLengthException {	//laps-klass ja vanem-klass vead, võivad olla koos
	{
		FileReader reader = new FileReader(new File("kasutajateFile.txt")); //
		BufferedReader bfReader = new BufferedReader(reader); //

		String minuRida;
		List<String> read = new ArrayList<>();
		int counter = 1;
		while ((minuRida = bfReader.readLine()) != null)
		{ 
			if (minuRida.length() >=3)
			{
			read.add(minuRida); 
			}else {
				throw new IncorrectStringLengthException ("Failis oli liiga lühike rida");
			}
			System.out.println(counter + "." + minuRida);
			counter++;
		}
		bfReader.close();	//kui close ei tee, siis jääb mälu lahti ja ei lähe vabaks
		reader.close();
		System.out.println("Listis on ridu " + read.size());

	}
	}
	public void vanem() throws FileNotFoundException, IOException, IncorrectStringLengthException {
		try {
			readMyLittleFile();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	}
	
