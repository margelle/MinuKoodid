package ee.bcs.ee.koolitusthrowdemo;

public class IncorrectStringLengthException extends Exception {//tüüpiliselt tehakse tema sees 2 konstruktorit
	public IncorrectStringLengthException() { 
		super(); //kutsub välja vanem-klassi constr
	}

	public IncorrectStringLengthException(String message) 
	{
		super(message);
	}
}