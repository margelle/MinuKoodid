package ee.bcs.ee.koolitusthrowdemo;

import java.io.FileNotFoundException;
import java.io.IOException;

public class MainThrow
{
	public static void main(String[] args)
	{
		MyThrowingClass myClass = new MyThrowingClass(); // uus klassi tehtud
		// myClass.readMyLittleFile(); //kutsume välja faili; näitab viga, vt
		// mulli
		// myClass.vanem();

		try
		{
			myClass.readMyLittleFile();
			myClass.vanem();
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException | RuntimeException e)
		{
			e.printStackTrace();
		} catch (IncorrectStringLengthException e)
		{
			System.out.println(e.getMessage());
//			{
//				// TODO Auto-generated catch block
//			e.printStackTrace();
//			}
		}
	}
}
