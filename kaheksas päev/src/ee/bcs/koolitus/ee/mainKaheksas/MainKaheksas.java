package ee.bcs.koolitus.ee.mainKaheksas;

import ee.bcs.koolitus.mammal.Human;
import ee.bcs.koolitus.mammal.Mammal;
import ee.bcs.koolitus.mammal.Student;

public class MainKaheksas
{

	public static void main(String[] args)
	{
		Mammal mammal = new Mammal("unknown"); //annan ette mingi tühja stringi
		mammal.breathesWithLungs();
		Human human = new Human();
		human.breathesWithLungs();
		human.talk();						//uus objekt
		Student student = new Student();
		student.breathesWithLungs();
	
		Mammal inimeneMammal = new Human(); //uus objekt, def-tud läbi üldisema tüüpi, kuid loodud läbi täpsema tüübi
		inimeneMammal.breathesWithLungs();	//võtab Humani versiooni, kuid meetodid on Mammalist
		//inimeneMammal.talk(); sellist verisooni ei saa kasutada, sest mammalil ei ole seda meetodit
	}

}
