package ee.bcs.koolitus.mammal;

public class Human extends Mammal{ //peale uue Mammali const tegemist näitas viga, selleks vaja täiendada superiga
	
	public Human(){
			super("Home sapiens"); 
	}
	
	@Override //Mammali meetod on ülekirjutatud
			public void breathesWithLungs(){ //kirjutan üle teise meetodi ja teen ta tühjaks; teine variant on muuta nähtavust
		System.out.println("Human breathes through nose with lunges");
	}
	public void talk()	//uus meetod lisaks nimega talk
	{
	System.out.println("This is a sentence!");
	}
}	
