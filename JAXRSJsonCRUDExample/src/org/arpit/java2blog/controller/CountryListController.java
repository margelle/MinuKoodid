package org.arpit.java2blog.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.arpit.java2blog.bean.Country;
import org.arpit.java2blog.service.CountryListService;

@Path("/countries1")
public class CountryListController
{
	CountryListService countryListService = new CountryListService();

	// GET-iga saan pärida andmeid//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Country> getCountries()
	{
		List<Country> listOfCountries = countryListService.getAllCountries();
		return listOfCountries;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Country getCountryById(@PathParam("id") int id)
	{
		return countryListService.getCountryById(id);
	}

	@GET
	@Path("/name/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Country getCountryByName(@PathParam("name") String name)
	{
		return countryListService.getCountryByName(name);
	}

	// kui soovin lisada/muuta veebirakenduses ridu, siis POST-iga//
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Country addCountry(Country country)
	{
		return countryListService.addCountry(country);
	}

	// @PUT
	// @Produces(MediaType.APPLICATION_JSON)
	// public Country updateCountry(Country country)
	// {
	// return countryListService.updateCountry(country);//
}
