package org.arpit.java2blog.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.arpit.java2blog.bean.Continent;
import org.arpit.java2blog.service.ContinentService;

@Path("/continents")
public class ContinentController
{
	ContinentService continentService = new ContinentService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Continent> getContinents()
	{
		List<Continent> listOfContinents = continentService.getAllContinents();
		return listOfContinents;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Continent getCountryById(@PathParam("id") int id)
	{
		return continentService.getContinentById(id);
	}

	@GET
	@Path("/name/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Continent getCountryByName(@PathParam("name") String name)
	{
		return continentService.getContinentByName(name);
	}
}
