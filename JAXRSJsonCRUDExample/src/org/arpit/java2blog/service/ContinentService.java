package org.arpit.java2blog.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.arpit.java2blog.bean.Continent;

public class ContinentService
{
	public List<Continent> getAllContinents()
	{
		List<Continent> listOfContinents = new ArrayList<>(); // tekitan tühja
																// listi//
		Connection connection = DBConnectionHandling.createConnection(); // loon
																			// ühenduse
																			// ab-ga//
		try (Statement statement = connection.createStatement(); // haakab
																	// koostama
																	// päringut,
																	// teeb
																	// statement
																	// objekti//
				ResultSet resultSet = statement.executeQuery("SELECT * FROM country_management.continent;");) // vali
																												// kõik
																												// tulemused,
																												// mis
																												// tabelist
																												// tulevad
																												// select*//
		{
			while (resultSet.next())
			{
				listOfContinents.add(new Continent(resultSet.getInt("continent_id"), resultSet.getString("name"))); // iga
																													// objekti
																													// kohta
																													// loon
																													// uue
																													// continendi//
			}
		} catch (SQLException e)
		{

			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return listOfContinents;
	}

	public Continent getContinentById(int continentId)
	{
		Continent continent = null;
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"SELECT * FROM country_management.continent WHERE continent_id=" + continentId + ";");)
		{
			while (resultSet.next()) // TÜHJAS LISTIS SAAB JUURDE PANNA UUSI//
			{
				continent = new Continent(resultSet.getInt("continent_id"), resultSet.getString("name"));
			}
		} catch (SQLException e)
		{
			e.printStackTrace(); // annab soovitud väljundi//
		}
		DBConnectionHandling.closeConnection(connection);
		return continent;
	}

	public Continent getContinentByName(String name)
	{
		Continent continent = null;
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement
						.executeQuery("SELECT * FROM country_management.continent WHERE name='" + name + "';");)
		{
			while (resultSet.next())
			{
				continent = new Continent(resultSet.getInt("continent_id"), resultSet.getString("name"));
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return continent;
	}
}
