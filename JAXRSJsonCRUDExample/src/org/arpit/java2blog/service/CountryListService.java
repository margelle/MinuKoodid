package org.arpit.java2blog.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.arpit.java2blog.bean.Continent;
import org.arpit.java2blog.bean.Country;

public class CountryListService
{
	public List<Country> getAllCountries()
	{

		List<Country> listOfCountries = new ArrayList<>();

		// kutsub välja olemasoleva meetodi, ei ole vaja minna SQL midagi
		// muutma//
		ContinentService continentService = new ContinentService();
		List<Continent> continents = continentService.getAllContinents();

		Connection connection = DBConnectionHandling.createConnection();

		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * FROM country_management.country;");)
		{
			while (resultSet.next())
			{
				// listOfCountries.add(new
				// Country(resultSet.getInt("country_id"),
				// resultSet.getString("name"),
				// resultSet.getLong("population")));

				Country country = new Country(resultSet.getInt("country_id"), resultSet.getString("name"),
						resultSet.getLong("population"));
				for (Continent continent : continents)
				{
					if (continent.getId() == resultSet.getInt("continent_id"))
					{
						country.setContinent(continent);
					}
				}
				listOfCountries.add(country);
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return listOfCountries;

	}

	public Country getCountryById(int id)
	{
		// TODO Auto-generated method stub
		return null;
	}

	public Country getCountryByName(String name)
	{
		// TODO Auto-generated method stub
		return null;
	}

	public Country addCountry(Country country)
	{
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();)
		{
			statement.executeUpdate("INSERT INTO country_management.country "
					+ "(name, population, continent_id) VALUES ('" + country.getCountryName() + "' , "
					+ country.getPopulation() + ", " + country.getContinent().getId() + ");");
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return country;
	}
}
