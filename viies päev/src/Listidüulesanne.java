import java.util.ArrayList;
import java.util.List;

public class Listidüulesanne
{

	// ÜL List
	public static void main(String[] args)
	{
		// taoliste listide arv teine teise sees pole ette määratud; loodud
		// listid andmetega.
		List<List<String>> riigidLinnadega = new ArrayList<>(); // so suurem
																// list kõikide
																// riikide kohta
		List<String> eesti = new ArrayList<>(); // so väiksem list ühe riigi
												// kohta
		eesti.add("Tallinn");
		eesti.add("Tallinn");
		eesti.add("Tallinn");
		eesti.add("Eesti");
		riigidLinnadega.add(eesti);

		List<String> soome = new ArrayList<>();
		soome.add("Helsinki");
		soome.add("Helsingi");
		soome.add("Helsinki");
		soome.add("Soome");

		riigidLinnadega.add(soome);

		List<String> lati = new ArrayList<>();
		lati.add("Riga");
		lati.add("Riia");
		lati.add("Riga");
		lati.add("Läti");
		riigidLinnadega.add(lati);

		for (List<String> riikLinnadega : riigidLinnadega)
		{ // et saaks väikestest listidest kätte suuremast listist ;//listide
			// korral kasutatakse funktsiooni GET!
			System.out.println(riikLinnadega.get(1)); // see for juba teab, et
														// suurema listi sees
														// onväiksemad listid ja
														// teab, et need on
														// String tüübilised

			System.out.println("Riik " + riikLinnadega.get(3) + ": pealinn " + riikLinnadega.get(0)
					+ ", inglise keeles - " + riikLinnadega.get(1) + ", kohalikus keeles - " + riikLinnadega.get(2));
		}

		System.out.println();

		System.out.println();

		for (List<String> riik : riigidLinnadega)
		{
			riik.add(0, riik.get(3)); // lisatud juurde veerga
			riik.remove(4); // kustutan lisatud veeru ära
			System.out.println(riik.get(0));
		}
		// automaatselt lisamisega Helsingfors
		riigidLinnadega.get(1).add(3, "Helsingfors");
		System.out.println(riigidLinnadega.get(1).get(3));
		for (List<String> riikLinnadega : riigidLinnadega)
		{
			int reaPikkus = riikLinnadega.size();
			for (int kohalikudNimed = 3; kohalikudNimed < reaPikkus; kohalikudNimed++)
			{
				if (kohalikudNimed < reaPikkus - 1)
				{
					System.out.print(riikLinnadega.get(kohalikudNimed) + "; ");
				} else
				{
					System.out.print(riikLinnadega.get(kohalikudNimed));
				}
			}
			System.out.println();
		}

	}

}