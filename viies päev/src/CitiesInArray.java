public class CitiesInArray {

	public static void main(String[] args) {

		String[][] linnad = new String[196][4];
		// ül 3

		linnad[0][0] = "Helsinki";
		linnad[0][1] = "Helsingi";
		linnad[0][2] = "Helsinki";
		linnad[0][3] = "Soome";

		linnad[1][0] = "Tallinn";
		linnad[1][1] = "Tallinn";
		linnad[1][2] = "Tallinn";
		linnad[1][3] = "Eesti";

		linnad[2][0] = "Riga";
		linnad[2][1] = "Riia";
		linnad[2][2] = "Riga";
		linnad[2][3] = "Läti";

		linnad[3][0] = "Vilnius";
		linnad[3][1] = "Vilnius";
		linnad[3][2] = "Vilnius";
		linnad[3][3] = "Leedu";

		linnad[4][0] = "Budapest";
		linnad[4][1] = "Budapest";
		linnad[4][2] = "Budapest";
		linnad[4][3] = "Ungari";

		// ül 4
		System.out.println(linnad[0][1]);
		System.out.println(linnad[1][1]);
		System.out.println(linnad[2][1]);
		System.out.println(linnad[3][1]);
		System.out.println(linnad[4][1]);

		// ül 5
		System.out.println("Riik - " + linnad[0][3] + "; pealinn - " + linnad[0][1] + "; inglise keeles - "
				+ linnad[0][0] + "; kohalikus keeles - " + linnad[0][2] + ".");
		System.out.println("Riik - " + linnad[1][3] + "; pealinn - " + linnad[1][1] + "; inglise keeles - "
				+ linnad[1][0] + "; kohalikus keeles - " + linnad[1][2] + ".");
		System.out.println("Riik - " + linnad[2][3] + "; pealinn - " + linnad[2][1] + "; inglise keeles - "
				+ linnad[2][0] + "; kohalikus keeles - " + linnad[2][2] + ".");
		System.out.println("Riik - " + linnad[3][3] + "; pealinn - " + linnad[3][1] + "; inglise keeles - "
				+ linnad[3][0] + "; kohalikus keeles - " + linnad[3][2] + ".");
		System.out.println("Riik - " + linnad[4][3] + "; pealinn - " + linnad[4][1] + "; inglise keeles - "
				+ linnad[4][0] + "; kohalikus keeles - " + linnad[4][2] + ".");

		// ül 6, tegin uue array, variant oli muuta ka esimest andmemassiivi

		String[][] cities = new String[196][];
		cities[0] = new String[5]; //siin on 5 rida, nurksulgudes määrad ära elementide arvu
		cities[0][0] = "Helsinki";
		cities[0][1] = "Helsingi";
		cities[0][2] = "Helsinki";
		cities[0][3] = "Soome";
		cities[0][4] = "Helsingfors";

		cities[1] = new String[4]; //siin on 4 rida
		cities[1][0] = "Tallinn";
		cities[1][1] = "Tallinn";
		cities[1][2] = "Tallinn";
		cities[1][3] = "Eesti";

		cities[2] = new String[4];
		cities[2][0] = "Riga";
		cities[2][1] = "Riia";
		cities[2][2] = "Riga";
		cities[2][3] = "Läti";

		cities[3] = new String[4];
		cities[3][0] = "Vilnius";
		cities[3][1] = "Vilnius";
		cities[3][2] = "Vilnius";
		cities[3][3] = "Leedu";

		cities[4] = new String[4];
		cities[4][0] = "Budapest";
		cities[4][1] = "Budapest";
		cities[4][2] = "Budapest";
		cities[4][3] = "Ungari";

		for (String[] city : cities) {
			if (city != null) {
				System.out.println(city[2]);
			} else {
				break;
			}
			for (String[] countryCities : cities) {

				if (countryCities == null) {
					break;
				}
				System.out.print("Riik -" + countryCities[0] + ": pealinn - " + countryCities[2] + "; inglise keeles - "
						+ countryCities[1] + ", kohalikus keeles - ");

				int rowLength = countryCities.length;

				for (int localNames = 3; localNames < rowLength; localNames++) {

					if (localNames < rowLength - 1) {
						System.out.print(countryCities[localNames] + "; ");
					} else {
						System.out.print(countryCities[localNames]);
					}
				}
				System.out.println();

				// ÜL 7 automaatlselt muudab country nime asukohta proge; leitav
				// gitlabist

			for(String[] countryCity : cities){
				if (countryCity == null) {
					break;
			}
			String[] tempCountyCityRow = new String[countryCity.length];
			tempCountryCityRow[0] = countryCity[countryCity.length-1];
			for(int i= 1; i<countryCity.length-2;i++){
				tempCountryCityRow[i] = countryCity[i-1];
			}
			countryCity = tempCountryCityRow;
			}
			
			}
		}
	}
}
	
	

