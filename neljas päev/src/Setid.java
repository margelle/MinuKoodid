import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ThreadLocalRandom;

public class Setid {

	public static void main(String[] args) {
		// järjestab sõnad tähestikus jrk-s ja loeb kokku mitu elementi väljastatud on. 
		//Kui on ühte sõna/elementi mitu korda, siis ta arvestab seda ühekordselt, sest no sarnased.

		Set<String> simpleSet = new TreeSet<>();
		simpleSet.add("text");
		simpleSet.add("alphabet");
		simpleSet.add("horse");
		simpleSet.add("subtext");
		simpleSet.add("vague");
		simpleSet.add("yyyy");
		simpleSet.add("aaaaaa");
		simpleSet.add("text");
		simpleSet.forEach(t -> {
			
			System.out.println(t);
		});
		System.out.println("Set Size is " + simpleSet.size()+ ".\n" );
	
		//mida suutrem vahemik, seda suurem on tõenosus, et tekib väiksem arv elemente; 
		//TreeSet võtab arvesse ühekordsed ja sorteeritud elemendid. Kasut Random funktsiooni, 
		//siis ei ole naabrite tulemused sarnased. i-ga ütled, mitut elementi ta tahab. String on tähestik, kui oleks Integer, siis oleks numbrilises jrk-s.
		Set <String>tekstiList = new TreeSet<>();
		int i = 0;
		while(i<10) {
			tekstiList.add("Seti objekt " + ThreadLocalRandom.current().nextInt(0, 50));
		i++;
		}
		for(String setiElement: tekstiList){
			System.out.println(setiElement);
	
			
		//muutsin Stringi Integeriks, annab loetelu numbtrite jrk-s, väärtus antakse i=1, sest eelnevalt on määratud ära tüüp, millega on tegemist.	
		Set <Integer> loetelu = new TreeSet<>();
		i = 0;
		while(i<10) {
				loetelu.add(ThreadLocalRandom.current().nextInt(0, 50));
		i++;
		}
		for(Integer arvud: loetelu){
				System.out.println(arvud);
		
		}
			}
		}
	}

