
public class forEachTsükkel {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String[][] tabel ={{"Mati", "Tallinn"}, {"Kati", "Pärnu"}, {"Mari", "Narva"}, {"Jüri", "Türi"}};
	
		
		//saab aru, et selles tabelis on mingi hulk ühemõõtmelisi asju, 
		//1.for - tsükkel võtab ükshaavale tabelis ühemõõtm. massiive ja iga rea kohta hakkan midagi tegema;
		//2. for - teab, et peab võtma järgmise rea 
		for(String[] rida : tabel){ 	// "Mati", "Tallinn"
			for(String lahter: rida){ 	//"Kati", "Pärnu"
				System.out.println(lahter);
			}
		}
	
	//	rida ja kahter on ise nimetatud, oluline on, et tabelinimega on seotud.
	String[][]tabelLoomad = new String[3][2];
	
	for (int i=0; i<tabelLoomad.length;i++){
		for(int j=0; j<tabelLoomad[i].length; j++){
			tabelLoomad[i][j] = "omadus reas " + (i+1) + " veerus "	+ (j + 1);
		}
		}
	for(String[] rida : tabelLoomad){ ///: on selle for-i tüübi omadus, iga seda tüüpi element sellest objektist teab, et see objekt kuulub seda tüüpi asjadest
		for(String lahter:rida){
			System.out.println(lahter);
		}	
		
	}
	
	
	
	}

}
