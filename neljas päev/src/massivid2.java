
public class massivid2 {

	public static void main(String[] args) {
		
	//1. for hakkab ridu lugema ja 2. for hakkab veerge lugema; rida on väiksem kui tabeli pikkus, 
	// st ridu kolm, loetakse al 0-st. Võtan tabelist vastava rea ja küsin, kus ma olen hetkel.  
		//NB! Massivile saab andmeid jurde lisada  vajadusel
		
String[][] tabel ={{"Mati", "Tallinn"}, {"Kati", "Pärnu"}, {"Mari", "Narva"}, {"Jüri", "Türi"}};
for (int rida = 0; rida < tabel.length; rida++){
	for (int veerg = 0; veerg < tabel[rida].length; veerg++){
		System.out.println(tabel[rida][veerg]);

		}
	}
// lisasin massivi juurde Jüri Türilt. ennem lisatud infota ei töödanud!
for (int rida =0; rida < tabel.length; rida++){
	System.out.println(tabel[rida][0] + " linnast " + tabel[rida][1]);
}

//ei pea kasutama kandilisi sulge, saba ka muud moodi otsida; iga tähe kohta on öeldud, mitmendal kohal ta on.
//tegelik tähe arvu suurendamine käib taht++-ga. charAt tagastab konkreetselt indeksilt, sellel kohal oleva tähe, 
//esimene täht on 0 indeksiga.
	String sona = "abracadabra";
	for (int taht = 0; taht < sona.length(); taht++){
		System.out.println((taht+1) + ". täht on '" + sona.charAt(taht) +"'");
	}
	
	
	System.out.println("------------------while abacadabra------------");
	//so teine variant eelneva harjutuse kohta
	int taheIndeks = 0;
	while(taheIndeks<sona.length()){
		System.out.println((taheIndeks + 1) + ". täht on '" + sona.charAt(taheIndeks) + "'");
		taheIndeks++;
		}
	}
}