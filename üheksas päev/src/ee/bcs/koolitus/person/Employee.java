package ee.bcs.koolitus.person;

import java.math.BigDecimal;

public class Employee extends Person
{
	public static final int MINIMUM_WORKING_AGE = 18; //
	private String name; // esmalt teen valmismuutujad
	private BigDecimal salary;

	public Employee(int age, String name, BigDecimal salary)
	{
		super(age);
		this.name = name;
		this.salary = salary;
	}

	public BigDecimal getSalary()
	{
		return salary;
	}

	public void setSalary(BigDecimal salary)
	{
		this.salary = salary;
	}

	public String getName()
	{
		return name;
	}

	
	public void setName(String name)
	{
		this.name = name;
	}

	private Employee()
	{
		super(MINIMUM_WORKING_AGE); // anname vanuse ette, et alla selle tööle
									// ei saa võtta
	}

}
