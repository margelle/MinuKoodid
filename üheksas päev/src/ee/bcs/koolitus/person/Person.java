package ee.bcs.koolitus.person;

public class Person			//kui inimene on oma väärtuse saanud, siis see ei muutu, so per inimene
{
	private int age;
	private final int id;  					// id saab väärtuse kui counteri abil objekti lood
	private static int counter = 100; 			//private static on kõikidel ühesugune väärtus
												// Lisa 1.2 loendur väli
	public Person (int age){
		this.id = counter++;
		//counter = counter+1;
		this.age = age; 
		
	
	}

	public int getAge()
	{
		return age;
	}

	public void setAge(int age)
	{
		this.age = age;
	}
	public int getId(){
		return this.id;
	}
}
