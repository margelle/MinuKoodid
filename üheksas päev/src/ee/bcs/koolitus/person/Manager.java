package ee.bcs.koolitus.person;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Manager extends Employee
{
	private String departement;			//alguses oli TreeMap tüüp, see oleks osakondade ajalugu alle shoidmud; kliendile ei meeldinud see andmetüüp ja muudame Stringiks.
	private List <Employee> departementStaff = new ArrayList<>();
	
	public Manager(int age, String name, BigDecimal salary, String departement){

		super(age, name, salary);	//anname Employee public const 3 muutujat; kuna teine on provate, siis seda kasutada ei saa
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm"); 	//määran ära kuupäeva väljastamise stiili
		Date today = new Date();		//teen uue kuupäeva, et saaks määrata väljastamisse
		System.out.println(dateFormat.format(today));
		this.departement = departement;
	}
	public String getDepartement(){
		return this.departement;
	}
	public void setDepartement(String departement){
		this.departement = departement;
	
	//ül 1 Lisa 1.1
	}
	public void addEmployeeToDepartement(Employee employee){		//töötajate lisamine
		
		departementStaff.add(employee);
	}
	public List<Employee> getDepartementStaff(){
		return this.departementStaff;
}
}