package MainÜheksasPäev;

import java.math.BigDecimal;
import java.util.List;

import ee.bcs.koolitus.person.Employee;
import ee.bcs.koolitus.person.Manager;
import ee.bcs.koolitus.person.Person;

public class MainÜheksas
{
	public static void main(String[] args)
	{
		Person p = new Person(18);
		Employee employee = new Employee(20, "Mari", BigDecimal.valueOf(800));
		
		
		System.out.println(p.getAge());
		System.out.println(employee.getName() + " is " + employee.getAge() + " years old and has salary " + employee.getSalary());
		Manager manager = new Manager(35, "Liisa", BigDecimal.valueOf(2000), " accounting");
		System.out.println(manager.getName() + " is " + manager.getAge() + "years old and has salary " + manager.getSalary() + manager.getDepartement());
	
		Employee employee2 = new Employee(45, "Kristajan", BigDecimal.valueOf(1500));
		
		manager.addEmployeeToDepartement(employee);
		manager.addEmployeeToDepartement(employee2);
		
		System.out.println("In her departement there are:");
		List<Employee> managerDepartementEmployees = manager.getDepartementStaff(); //tekitati uus muutuja
		for(Employee emp : managerDepartementEmployees){
			System.out.println(emp.getName());
		}
	
		System.out.println("p id = " + p.getId());
		System.out.println(employee.getName() + " id = " + employee.getId());
		System.out.println(manager.getName() + " id = " + manager.getId());
		System.out.println(employee2.getName() + " id = " + employee2.getId());
	}
}
