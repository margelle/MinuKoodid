package eee.bcs.koolitus.employee;

import java.math.BigDecimal;

public class Employee
{
	private BigDecimal salary = BigDecimal.ZERO; // loodud väli salary
	private String name;		//Lisa1  muutuja name loomine
	
	
		
	public Employee(){	//uus konstruktor ül 4 tegemiseks // ül 7 muutujate konstruktor oli public, muutsin private-ks
	
	}	
	public Employee(BigDecimal salary) //konstruktor
	{
		this.salary = salary;
	}

	// ül 3: getteri tüüpi pole vaja muuta, muudetakse settereid
	public BigDecimal getSalary()
	{
		return salary;
	}

	//ül 7 Lisa1 ; ül 7 Lisa3 muudan publicu -> private-ks
	public Employee(String name, BigDecimal salary){	//uus konstruktor Lisa1 sisendiks nimi ja palk
		this.name = name;
		this.salary = salary;
	
		
		
		
		// ül 2
	//public void setSalary(BigDecimal salary){ // loodud void meetodiga setter	
	//	this.salary = salary;
	

		//ül 3 vaja muuta void tüüpi setter Employee-ks, et tagastaks Employee objekti
		
	//public Employee setSalary(BigDecimal salary){
	
	//this.salary = salary;
	//return this;
	
	//Lisa 1  uued getterid ja setterid
		
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public void setSalary(BigDecimal salary)
	{
		this.salary = salary;
	}
	
	//Lisa 2
	@Override
	public String toString(){
		return "Employee " + name + " has salary " + salary + "."; //this.name ja this.salary on ka õige
	}
	
	public void changeSalary(BigDecimal newSalary)
	{ // newSalary on määratud reas 20-21
		setSalary(getSalary().add(newSalary)); // muutsin sulgudes väärtust, et arvutaks +10; newSalary kasutan selle asemel, et mitte muuta käistsi väärtust 
		System.out.println("changeSalary: " + getSalary());

		// 2 meetodit on sarnased, v.a. teisel on rohkem/eritüüpi muutujaid

	}
	// ül 7 tuleb 
	public void changeSalary()
	{ 
		changeSalary(BigDecimal.valueOf(100)); //siin on 2 meetodit ja tegemist oli over-loadiga
	//	employee.setSalary(employee.getSalary().add(BigDecimal.valueOf(100))); siin ära kommitud, sest uus kood on kirjutatud, vt rida 46
		System.out.println();

	
	}
}
