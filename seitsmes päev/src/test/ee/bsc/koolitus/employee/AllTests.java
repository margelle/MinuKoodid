package test.ee.bsc.koolitus.employee;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class) //suite.class vastutab, et paneb testi käivitamise alguskohaks
@SuiteClasses(
{EmployeeTest.class})
public class AllTests
{

	
	
}
