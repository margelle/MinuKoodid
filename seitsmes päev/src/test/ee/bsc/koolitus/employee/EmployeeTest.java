package test.ee.bsc.koolitus.employee;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

import eee.bcs.koolitus.employee.Employee;

public class EmployeeTest
{

	@Test
	public void testChangeSalaryUpdatesSalary() //tehti meetod test; annotatatsioon @ näitab, et sinu meetodis on olemas testimeetod, mille järgi ta otsimas käib
	{
		Employee testEmployee	 = new Employee("Mari", BigDecimal.valueOf(1000));
		testEmployee.changeSalary(BigDecimal.valueOf(100));
		Assert.assertEquals(BigDecimal.valueOf(1100), testEmployee.getSalary());
	}
	@Test
	public void testChangeSalaryUpdatesSalary1() //uus test
	{
		Employee testEmployee	 = new Employee();
		testEmployee.changeSalary(BigDecimal.valueOf(100));
		Assert.assertEquals(BigDecimal.valueOf(1100), testEmployee.getSalary());
	
	}
}