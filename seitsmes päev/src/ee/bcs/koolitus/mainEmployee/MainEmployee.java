package ee.bcs.koolitus.mainEmployee;

import java.math.BigDecimal;

import eee.bcs.koolitus.employee.Employee;

public class MainEmployee
{

	public static void main(String[] args)
	{
		Employee employee1 = new Employee("Lille Õis", BigDecimal.valueOf(1000));//nimi juurde kirjutatud ül 7 lisa2
		System.out.println(employee1.getSalary());

		
		changeSalary(employee1); // meetodi välja kutsumine

		System.out.println(employee1.getSalary());

		// ül 6 uus meetod mitme väärtusega
		changeSalary(employee1, BigDecimal.valueOf(10));
		System.out.println(employee1.getSalary());
		
		//ül 7 Lisa 1:
		Employee employeeWithName = new Employee("Mari", BigDecimal.valueOf(1200));
		System.out.println(employeeWithName.getName() + " palgaga " + employeeWithName.getSalary()); //küsib objektilt employeeWithName nime ja palka

	// ül 7 Lisa2 väljastamine
	System.out.println(employee1);
	}

	//private static void changeSalary(Employee employee)
	{ // void tüüpi meetod, nähtav ainult selles klassis, employee läheb
		// sisendiks
		// employee = new Employee(); uue set-iga muudame vana muutuja väärtust,
		// sellep on ära kommitud, muidu ei arvuta uut palka
		//employee.setSalary(employee.getSalary().add(BigDecimal.valueOf(100))); // muudad 	väärtust, et arvestaks palka +100
		//System.out.println("changeSalary: " + employee.getSalary());
	}

	
	//Siin on testimise meetodid eraldi välja toodud, testin lehel EmployeeTest.
	
	private static void changeSalary(Employee employee, BigDecimal newSalary)
	{ // newSalary on määratud reas 20-21
		employee.setSalary(employee.getSalary().add(newSalary)); // muutsin sulgudes väärtust, et arvutaks +10; newSalary kasutan selle asemel, et mitte muuta käistsi väärtust 
		System.out.println("changeSalary: " + employee.getSalary());

		// 2 meetodit on sarnased, v.a. teisel on rohkem/eritüüpi muutujaid

	}
	private static void changeSalary(Employee employee)
	{ 
		changeSalary(employee, BigDecimal.valueOf(100)); //siin on 2 meetodit ja tegemist oli over-loadiga
	//	employee.setSalary(employee.getSalary().add(BigDecimal.valueOf(100))); siin ära kommitud, sest uus kood on kirjutatud, vt rida 46
		System.out.println();

	
	}
	
}
