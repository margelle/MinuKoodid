package ee.bcs.koolitus.main;

import ee.bcs.koolitus.nahtavus.Nahtavus;

public class Main
{
	Nahtavus nahtavus = new Nahtavus();

	public void testiNahtavuseMuutujaid()
	{
		// System.out.println(nahtavus.klassiMuutujaPrivate); //nähtavus on
		// piiratud ja näitab punast, serllep kommitud
		// System.out.println(nahtavus.klassiMuutujaDefault);
		// System.out.println(nahtavus.klassiMuutujaProtected);
		System.out.println(nahtavus.klassiMuutujaPublic);
	}

	public static void main(String[] args)
	{
		//System.out.println(nahtavus.klassiMuutujaPrivate); // nähtavus on
															// piiratud ja
															// näitab punast,
															// serllep kommitud
		//System.out.println(nahtavus.klassiMuutujaDefault);
		//System.out.println(nahtavus.klassiMuutujaProtected);
		//System.out.println(nahtavus.klassiMuutujaPublic);

	}
}