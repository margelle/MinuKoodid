package ee.bcs.koolitus.vaiksem;

import ee.bcs.koolitus.nahtavus.Nahtavus;

public class NahtavuseAlamklassVaiksem extends Nahtavus
{
	Nahtavus nahtavus = new Nahtavus();
	
	public void testiNahtavuseMuutujaid(){
		//System.out.println(nahtavus.klassiMuutujaPrivate); //nähtavus on piiratud ja näitab punast, serllep kommitud
		//System.out.println(nahtavus.klassiMuutujaDefault);
		//System.out.println(nahtavus.klassiMuutujaProtected);
		System.out.println(nahtavus.klassiMuutujaPublic);
	}
}