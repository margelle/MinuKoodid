
public class Main
{

	public static void main(String[] args)
	{
		BeautifulClass beauty1 = new BeautifulClass("patience", 8);
		beauty1.printBeautyLevel(20); 	//nii kutsud seda meetodit beauty1.printBeautyLevel(), läheb konstruktorisse vaatama, mida ta tegema peab
	
		int calculatedValue = beauty1.calculatedValueLevel(20);				
		System.out.println("Calculated value is: " + calculatedValue);
		
		System.out.println(beauty1.getBeautyMeaning());
		beauty1.setBeautyMeaning("patience2");				//setter
		System.out.println(beauty1.getBeautyMeaning());		//getter
		
		System.out.println(beauty1.getMeasure());
		beauty1.setMeasure(456);
		System.out.println(beauty1.getMeasure());
		
		
		
		
		BeautifulClass beauty2 = new BeautifulClass();
		BeautifulClass beauty3 = new BeautifulClass("123",10);
		System.out.println(beauty1.beautyMeaning); 		// pole objektile väärtus antud, trükib null
		System.out.println(beauty2.beautyMeaning);
		System.out.println(beauty3.beautyMeaning);
		beauty1.beautyMeaning = "wisdom";				//objektile antud väärtus wisdom	
		System.out.println(beauty1.beautyMeaning);
		System.out.println(beauty2.beautyMeaning);
		System.out.println(beauty3.beautyMeaning);
	}

}



