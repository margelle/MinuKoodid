public class BeautifulClass
{
	public String beautyMeaning;
	public int measure;
	
	public BeautifulClass()	//uus default konstruktor
	{
		this.beautyMeaning = "beautySoul";
	
	}
	/**
	 * 
	 * @param beautyMeaning - how beauty is defined
	 * @param measure - how it is measured
	 */
	
	public BeautifulClass(String beautyMeaning, int measure)	//eemaldati konstruktor
	{
		this.beautyMeaning = beautyMeaning;
		this.measure=measure;
	}	
	public BeautifulClass(Integer beautyMeaning) 
	{
		this.beautyMeaning = beautyMeaning.toString();
	}

	
	// loome 2 erinevat public meetodit
	
	public void printBeautyLevel(int customMultiplier1) //void - tulemust ei tagasta
	{
		System.out.println("CustomMultiplier = " + customMultiplier1);
		System.out.println("BeautyLevel is: " + calculatedValueLevel(customMultiplier1));
		
										//(measure*10)); 	//measure on eraldi defineeritud üleval, sest on arvuline muutuja
	}										//+ calculatedBeautyLevel
	
	

	
	public int calculatedValueLevel(int customMultiplier2)	//int - tagastab arve, sellisel juhul peab olema return-käsk
	{
		return measure * customMultiplier2;	//returni järgi print olla ei saa! siin väljastab arvutamisega
	
	}
	//SETTERID JA GETTERID
	
	public String getBeautyMeaning(){
		return this.beautyMeaning;
	}
	
	public void setBeautyMeaning(String beautyMeaning){
		this.beautyMeaning = beautyMeaning;
	}

	public int getMeasure(){		//tagastab int-i
		return this.measure;
	}
	
	public void setMeasure(int measure){	//võtab int-i sisse; NB! kui see mootod on private, siis seda kasutada ei saa ja kaustad alumist varianti.
		this.measure = measure;
	}
	
	public void setMeasure(String measure) {		//Saab anda erinevat tüüpi muutujaid sisse; ütleb sulle, et näed siit tuleb muutuja ja tule tee väärtus ära
		setMeasure(Integer.parseInt(measure));
	}


//Over-load - sama meetod, erinev nimi ehk ülelaadimine
//Over-ride - 2 klassi pärilusega seotud, lood samanimelise meetodi, mis on vanemas klassis olemas, kuid sisu on sama 

	
}




	



