package ee.bcs.koolitus.trycatch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainTC
{
	public static void main(String[] args){
		try(FileReader reader = new FileReader(new File("kasutajatöeFile.txt")); //FileReader otsib üles kasutatava faili, loeb märkhaaval ära
				BufferedReader bfReader= new BufferedReader(reader)){	//lisatud teine reader bufferedReader juurde, et töötaksid koos
			//buffered reader otsib üles character Array ja otsib välja read
			String minuRida;
			List<String> read = new ArrayList<>();
			int counter = 1;
			while((minuRida = bfReader.readLine())!= null){  //loeb järgmise reani seni kaua kuni järgmisi ridu pole; NB! tühi rida on ka rida; 
				read.add(minuRida);		//olen rea väärtuse kätte saanud, kirjutatakse iga whilega üle ja pannakse listi
				System.out.println(counter + "." + minuRida);
				counter++;	//vajalik, et saaks numbrid ette panna
			}
			//char[] fileContent = new char[500];	//so char Array	1. variant kasutas seda
			//reader.read(fileContent);
			//System.out.println(fileContent);
		System.out.println("Listis on ridu " + read.size());	
		} catch (FileNotFoundException e) { System.out.println("if you got it, go and have a lunch");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
