package ee.bcs.koolitus.Main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Main
{
	public static void main(String[] args)
	{
		FileWriter writer = null;

		try
		{ // try-cathc method
			writer = new FileWriter(new File("minuUusFail.txt"), true);
			int i = 1;
			while (i <= 5)
			{
				writer.append("This ise a " + i + ". sentence \n");
				i++;
			}
			writer.flush();
			writer.close();
		} catch (IOException e)
		{
			e.printStackTrace();

		} catch (Exception e)
		{ // teist tüüpi erand
			System.out.println(e.getMessage());

			try
			{
				writer.close();
			} catch (IOException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} finally
			{
				
			}

		}
		// so 2. meetod ilma close meetodita try with resource - annad
		// objekti loomise sisse, closemise rea võtad ära
		try (FileWriter writer1 = new FileWriter(new File("minuUusFail.txt"), true))
		{
			int i = 1;
			while (i <= 5)
			{
				writer1.append("This is a " + i + ". sentence \n");
				i++;
			}
			writer1.flush();

		} catch (IOException e)
		{ // töötab kui tuleb IOExceptions, kasutame kasutajale tagasiside
			// andmiseks või süsteemile vea viitamiseks
			e.printStackTrace();

		}
	}
}
